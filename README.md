# Homework assignment 

### Project installation (dockerized version)

Project can be executed as docker-compose yaml files. There are to distribution of the project:
* For development purposes (docker-compose-dev.yml)
* For production purposes (docker-compose-prod.yml)

#### Building docker images

First the maven project must be built with an appropriate profile (dev or prod):

#### Dev 
    mvn clean install -P dev
    mvn docker:build -f pom.xml -P dev
    
#### Prod 
    mvn clean install -P prod
    mvn docker:build -f pom.xml -P prod
    
This will create homework-dev and homework-prod images 

#### Application execution using docker-compose files

#### Dev
    docker-compose -f docker-compose-dev.yml up
#### Prod
    docker-compose -f docker-compose-prod.yml up
    
To run the application mysql database is needed (it is included as an image in compose files), so the first execution of 
yaml files will take longer time.


### Project installation (non dockerized version)

To install and execute non-dockerized version, standalone mysql database is needed. 
Database aliases db or (and) db-prod should be added to /etc/hosts file

#### Dev

To start the application a development database is needed:

    spring.datasource.url=jdbc:mysql://db:3306/dev_products?serverTimezone=UTC
    spring.datasource.username=user
    spring.datasource.password=pwd

##### Execution

    mvn spring-boot:run -Dspring-boot.run.profiles=dev -f pom.xml

or given the jar file:
    
    java -Dspring.profiles.active=dev -jar homework-0.0.1-SNAPSHOT.jar 
    
#### Prod
    
To start the application a production database is needed:

    spring.datasource.url=jdbc:mysql://db-prod:3306/prod_products?serverTimezone=UTC
    spring.datasource.username=prod_user
    spring.datasource.password=prod_pwd
    
##### Execution

    mvn spring-boot:run -Dspring-boot.run.profiles=prod -f pom.xml

or given the jar file:

    java -Dspring.profiles.active=prod -jar homework-0.0.1-SNAPSHOT.jar



### Rest endpoints description

After application execution, it is possible to get REST endpoint description at:

http://localhost:8081/swagger-ui.html#/product-rest-controller

(port 8081 if executed from docker-compose-* files, 8080 if standalone).
