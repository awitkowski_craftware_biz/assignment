package biz.craftware.homework.core.domain.order;

import biz.craftware.homework.controller.order.OrderRestController;
import biz.craftware.homework.core.service.order.OrderService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.math.BigDecimal;

import static org.mockito.ArgumentMatchers.any;


@WebMvcTest(value = OrderRestController.class)
public class OrderRestControllerTest {

    public static final String UID = "12345";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private OrderService orderService;

    @Test
    public void shouldReturnOrderTotalAmount() throws Exception {
        //given
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        Mockito.when(orderService.getTotalOrderAmount(any())).thenReturn(new BigDecimal(100));

        //when
        String urlTemplate = String.format("/orders/%s/sum", UID);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(urlTemplate).accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        //then
        Assertions.assertThat(result.getResponse().getContentAsString()).isEqualTo("100");
    }

}
