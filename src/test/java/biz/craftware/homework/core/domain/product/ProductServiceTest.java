package biz.craftware.homework.core.domain.product;


import biz.craftware.homework.AdaptersMockConfiguration;
import biz.craftware.homework.controller.product.dto.ProductDto;
import biz.craftware.homework.core.domain.DomainTestsUtil;
import biz.craftware.homework.core.domain.Product;
import biz.craftware.homework.core.service.product.ProductService;
import biz.craftware.homework.exceptions.InvalidRequestException;
import biz.craftware.homework.repository.ProductRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;

import static biz.craftware.homework.core.domain.DomainTestsUtil.createProduct;
import static biz.craftware.homework.core.domain.DomainTestsUtil.createProductDto;
import static org.mockito.ArgumentMatchers.any;

@SpringBootTest(classes = AdaptersMockConfiguration.class)
public class ProductServiceTest {

    @Autowired
    private ProductService testProductService;

    @Autowired
    private ProductRepository productRepository;

    @Test
    public void shouldGetAllProducts() {
        //given
        List<Product> products = DomainTestsUtil.prepareSampleData();
        Mockito.when(productRepository.findAllByDeletedIsFalse()).thenReturn(products);

        //when
        List<ProductDto> result = testProductService.getAllProducts();

        //then
        Assertions.assertThat(result).isNotNull();
        Assertions.assertThat(result).isNotEmpty();
        Assertions.assertThat(result).hasSize(2);
        Mockito.verify(productRepository, Mockito.times(1)).findAllByDeletedIsFalse();
    }

    @Test
    public void shouldInsertProduct() {
        //given
        ProductDto productDto = createProductDto();
        Mockito.when(productRepository.findBySku(productDto.getSku())).thenReturn(Optional.empty());
        Mockito.when(productRepository.save(any())).thenReturn(new Product());

        //when
        ProductDto product = testProductService.addProduct(productDto);

        //then
        Mockito.verify(productRepository, Mockito.times(1)).findBySku(any());
        Mockito.verify(productRepository, Mockito.times(1)).save(any());
        Assertions.assertThat(product).isNotNull();
    }

    @Test
    public void shouldThrowProductSkuAlreadyPresentException() {

        //given
        ProductDto productDto = createProductDto();
        Mockito.when(productRepository.findBySku(productDto.getSku())).thenReturn(Optional.of(new Product()));

        //when
        InvalidRequestException exception = org.junit.jupiter.api.Assertions.assertThrows(InvalidRequestException.class, () -> testProductService.addProduct(productDto));

        //then
        Assertions.assertThat(exception).hasMessage(String.format(InvalidRequestException.SKU_IS_ALREADY_PRESENT, productDto.getSku()));
    }

    @Test
    public void shouldUpdateExistingProduct() {
        //given
        ProductDto productDto = createProductDto();
        Product product = createProduct();
        Mockito.when(productRepository.findBySku(productDto.getSku())).thenReturn(Optional.of(product));
        Mockito.when(productRepository.save(any())).thenReturn(product);

        //when
        ProductDto responseDto = testProductService.updateProduct(productDto, productDto.getSku());

        //then
        Assertions.assertThat(responseDto).isNotNull();
        Mockito.verify(productRepository, Mockito.times(1)).findBySku(any());
        Mockito.verify(productRepository, Mockito.times(1)).save(any());
    }

    @Test
    public void shouldThrowCreateNoProductSkuPresentExceptionWhenUpdating() {

        //given
        ProductDto productDto = createProductDto();
        Mockito.when(productRepository.findBySku(productDto.getSku())).thenReturn(Optional.empty());

        //when
        InvalidRequestException exception = org.junit.jupiter.api.Assertions.assertThrows(InvalidRequestException.class,
                () -> testProductService.updateProduct(productDto, productDto.getSku()));

        //then
        Assertions.assertThat(exception).hasMessage(String.format(InvalidRequestException.SKU_IS_NOT_PRESENT, productDto.getSku()));
    }

    @Test
    public void shouldDeleteProduct() {
        //given
        ProductDto productDto = createProductDto();
        Product product = createProduct();
        Mockito.when(productRepository.findBySku(productDto.getSku())).thenReturn(Optional.of(product));

        //when
        testProductService.deleteProduct(productDto.getSku());

        //then
        Mockito.verify(productRepository, Mockito.times(1)).save(any());
        Assertions.assertThat(product.getDeleted()).isTrue();
    }

    @Test
    public void shouldThrowCreateNoProductSkuPresentExceptionWhenDeleting() {

        //given
        ProductDto productDto = createProductDto();
        Mockito.when(productRepository.findBySku(productDto.getSku())).thenReturn(Optional.empty());

        //when
        InvalidRequestException exception = org.junit.jupiter.api.Assertions.assertThrows(InvalidRequestException.class, () -> testProductService.deleteProduct(productDto.getSku()));

        //then
        Assertions.assertThat(exception).hasMessage(String.format(InvalidRequestException.SKU_IS_NOT_PRESENT, productDto.getSku()));
    }

}
