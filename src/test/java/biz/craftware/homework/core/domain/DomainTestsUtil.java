package biz.craftware.homework.core.domain;

import biz.craftware.homework.controller.order.dto.OrderDto;
import biz.craftware.homework.controller.product.dto.ProductDto;
import com.google.common.collect.Sets;
import org.assertj.core.util.Lists;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

public class DomainTestsUtil {

    private static final String SKU_1 = "sku1";
    private static final String SKU_2 = "sku2";
    private static final String SKU_3 = "sku3";
    private static final String SKU_4 = "sku4";

    private static final String SAMPLE_PROD_1 = "sampleProd1";
    private static final String SAMPLE_PROD_2 = "sampleProd2";
    private static final String PRICE1 = "10.3";
    private static final String PRICE2 = "234.4";
    private static final String PRICE3 = "40.3";
    private static final String PRICE4 = "234.4";
    private static final String CUSOMTER_EMAIL = "a@a.pl";
    private static final String ORDER_ID = "1122";
    private static final String ORDER_ID_2 = "112222";


    public static ProductDto createProductDto() {
        return ProductDto.builder().name(SAMPLE_PROD_1).price(new BigDecimal(PRICE1)).sku(SKU_1).build();
    }

    public static Product createProduct() {
        return createProduct(SKU_1);
    }

    public static Product createProduct(String sku) {
        return Product.builder().name(SAMPLE_PROD_1).price(new BigDecimal(PRICE1)).sku(sku).build();
    }

    public static Product createProduct(String sku, BigDecimal price) {
        return Product.builder().name(SAMPLE_PROD_1).price(price).sku(sku).build();
    }

    public static List<Product> prepareSampleData() {
        List<Product> products = Lists.newArrayList(
                Product.builder().name(SAMPLE_PROD_1).price(new BigDecimal(PRICE1)).sku(SKU_1).build(),
                Product.builder().name(SAMPLE_PROD_2).price(new BigDecimal(PRICE2)).sku(SKU_2).build());
        return products;
    }

    public static OrderDto createOrderDto() {
        return createOrderDto(Sets.newHashSet(SKU_1, SKU_2, SKU_3));
    }

    public static OrderDto createOrderDto(Set<String> skus) {
        return OrderDto.builder()
                .cusomterEmail(CUSOMTER_EMAIL).orderId(ORDER_ID)
                .products(skus)
                .build();
    }

    public static Order createOrder(OrderDto orderDto) {
        Order order = Order.builder()
                .customerEmail(orderDto.getCusomterEmail())
                .orderUid(orderDto.getOrderId())
                .products(Sets.newHashSet())
                .build();
        return order;
    }

    public static Order createOrder() {
        return  Order.builder()
                .customerEmail(CUSOMTER_EMAIL)
                .orderUid(ORDER_ID)
                .products(Sets.newHashSet(createProduct(SKU_1, new BigDecimal(PRICE1)), createProduct(SKU_2, new BigDecimal(PRICE2))))
                .build();
    }

    public static List<Order> createOrders() {

        Order order1 = Order.builder()
                .customerEmail(CUSOMTER_EMAIL)
                .orderUid(ORDER_ID)
                .products(Sets.newHashSet(createProduct(SKU_1, new BigDecimal(PRICE1)), createProduct(SKU_2, new BigDecimal(PRICE2))))
                .build();

        Order order2 = Order.builder()
                .customerEmail(CUSOMTER_EMAIL)
                .orderUid(ORDER_ID_2)
                .products(Sets.newHashSet(createProduct(SKU_3, new BigDecimal(PRICE3)), createProduct(SKU_4, new BigDecimal(PRICE4))))
                .build();

        return Lists.newArrayList(order1, order2);
    }
}
