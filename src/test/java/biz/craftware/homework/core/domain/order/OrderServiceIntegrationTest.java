package biz.craftware.homework.core.domain.order;

import biz.craftware.homework.controller.order.dto.OrderDto;
import biz.craftware.homework.core.domain.DomainTestsUtil;
import biz.craftware.homework.core.service.order.OrderService;
import biz.craftware.homework.exceptions.InvalidRequestException;
import com.google.common.collect.Sets;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@Sql("/integrationTestData.sql")
@ActiveProfiles({"test"})
@Transactional
public class OrderServiceIntegrationTest {

    public static final String DATE_PATTERN = "yyyy-MM-dd HH:mm:ss";
    @Autowired
    private OrderService orderService;

    @Test
    public void shouldPlaceOrder() {
        //given
        OrderDto orderDto = DomainTestsUtil.createOrderDto(Sets.newHashSet("SKU555", "SKU666"));

        //when
        OrderDto responseDto = orderService.addOrder(orderDto);

        //then
        Assertions.assertThat(responseDto).isNotNull();
        Assertions.assertThat(responseDto.getProducts()).isNotEmpty();
        Assertions.assertThat(responseDto.getProducts()).hasSize(2);
    }

    @Test
    public void shouldThrowProductAlreadyAssignedException() {
        //given
        OrderDto orderDto = DomainTestsUtil.createOrderDto(Sets.newHashSet("SKU111", "SKU666"));

        //when
        InvalidRequestException exception = assertThrows(InvalidRequestException.class, () -> orderService.addOrder(orderDto));

        //then
        Assertions.assertThat(exception).hasMessage(InvalidRequestException.SKU_IS_IN_ANOTHER_ORDER);
    }

    @Test
    public void shouldReturnAllOrderBetweenDates() {
        //given
        String strFrom = "2011-01-01 00:01:01";
        String strTo = "2019-01-01 00:01:01";

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_PATTERN);
        LocalDateTime dateFrom = LocalDateTime.parse(strFrom, formatter);
        LocalDateTime dateTo = LocalDateTime.parse(strTo, formatter);

        //when
        List<OrderDto> orders = orderService.getAllOrders(dateFrom, dateTo);

        //then
        Assertions.assertThat(orders).isNotNull();
        Assertions.assertThat(orders).hasSize(2);
    }

    @Test
    public void shouldReturnOneOrder() {
        //given
        String strFrom = "2011-01-01 00:01:01";
        String strTo = "2011-03-02 00:01:01";

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_PATTERN);
        LocalDateTime dateFrom = LocalDateTime.parse(strFrom, formatter);
        LocalDateTime dateTo = LocalDateTime.parse(strTo, formatter);

        //when
        List<OrderDto> orders = orderService.getAllOrders(dateFrom, dateTo);

        //then
        Assertions.assertThat(orders).isNotNull();
        Assertions.assertThat(orders).hasSize(1);
    }

    @Test
    public void shouldNotReturnAnyOrders() {
        //given
        String strFrom = "2011-01-01 00:01:01";
        String strTo = "2011-01-02 00:01:01";

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_PATTERN);
        LocalDateTime dateFrom = LocalDateTime.parse(strFrom, formatter);
        LocalDateTime dateTo = LocalDateTime.parse(strTo, formatter);

        //when
        List<OrderDto> orders = orderService.getAllOrders(dateFrom, dateTo);

        //then
        Assertions.assertThat(orders).isNotNull();
        Assertions.assertThat(orders).isEmpty();
    }

}
