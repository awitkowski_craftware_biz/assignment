package biz.craftware.homework.core.domain.order;

import biz.craftware.homework.AdaptersMockConfiguration;
import biz.craftware.homework.controller.order.dto.OrderDto;
import biz.craftware.homework.core.domain.DomainTestsUtil;
import biz.craftware.homework.core.domain.Order;
import biz.craftware.homework.core.domain.Product;
import biz.craftware.homework.core.service.order.OrderService;
import biz.craftware.homework.exceptions.InvalidRequestException;
import biz.craftware.homework.repository.OrderRepository;
import biz.craftware.homework.repository.ProductRepository;
import org.assertj.core.api.Assertions;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;

@SpringBootTest(classes = AdaptersMockConfiguration.class)
public class OrderServiceTest {

    @Autowired
    private OrderService testOrderService;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ProductRepository productRepository;

    @Test
    public void shouldPlaceOrder() {
        //given
        OrderDto orderDto = DomainTestsUtil.createOrderDto();
        Mockito.when(orderRepository.findByOrderUid(orderDto.getOrderId())).thenReturn(Optional.empty());
        Mockito.when(productRepository.findAllBySkuInAndOrderIsNotNull(orderDto.getProducts())).thenReturn(Optional.empty());
        Mockito.when(orderRepository.save(any())).thenReturn(DomainTestsUtil.createOrder(orderDto));

        //when
        OrderDto responseDto = testOrderService.addOrder(orderDto);

        //then
        Assertions.assertThat(responseDto).isNotNull();
        Mockito.verify(orderRepository, Mockito.times(1)).save(any());
    }

    @Test
    public void shouldThrowProductAlreadyAssignedException() {
        //given
        OrderDto orderDto = DomainTestsUtil.createOrderDto();
        Mockito.when(orderRepository.findByOrderUid(orderDto.getOrderId())).thenReturn(Optional.empty());
        Mockito.when(productRepository.findAllBySkuInAndOrderIsNotNull(orderDto.getProducts()))
                .thenReturn(Optional.of(Lists.newArrayList(new Product())));

        //when
        InvalidRequestException exception = org.junit.jupiter.api.Assertions.assertThrows(InvalidRequestException.class,
                () -> testOrderService.addOrder(orderDto));

        //then
        Assertions.assertThat(exception).hasMessage(InvalidRequestException.SKU_IS_IN_ANOTHER_ORDER);
    }

    @Test
    public void shouldComputeTotalOrderAmount() {
        //given
        Order order = DomainTestsUtil.createOrder();
        Mockito.when(orderRepository.findByOrderUid(order.getOrderUid())).thenReturn(Optional.of(order));

        //when
        BigDecimal totalOrderAmount = testOrderService.getTotalOrderAmount(order.getOrderUid());

        //then
        Assertions.assertThat(totalOrderAmount).isNotNull();
        Assertions.assertThat(totalOrderAmount.floatValue()).isEqualTo(244.7f);
        Mockito.verify(orderRepository, Mockito.times(1)).findByOrderUid(order.getOrderUid());
    }

    @Test
    public void shouldThrowNoOrderPresentException() {
        //given
        OrderDto orderDto = DomainTestsUtil.createOrderDto();
        Mockito.when(orderRepository.findByOrderUid(orderDto.getOrderId())).thenReturn(Optional.empty());

        //when
        InvalidRequestException exception = org.junit.jupiter.api.Assertions.assertThrows(InvalidRequestException.class,
                () -> testOrderService.getTotalOrderAmount(orderDto.getOrderId()));

        //then
        Assertions.assertThat(exception).hasMessage(String.format(InvalidRequestException.ORDER_ID_IS_NOT_PRESENT, orderDto.getOrderId()));
    }

    @Test
    public void shouldReturnOrdersBeetweenDates() {
        //given
        List<Order> orders = DomainTestsUtil.createOrders();
        Mockito.when(orderRepository.getAllByDateCreatedBetween(any(), any())).thenReturn(orders);

        //when
        List<OrderDto> allOrders = testOrderService.getAllOrders(LocalDateTime.now(), LocalDateTime.now());

        //then
        Assertions.assertThat(allOrders).isNotNull();
        Assertions.assertThat(allOrders).hasSize(2);
        Mockito.verify(orderRepository, Mockito.times(1)).getAllByDateCreatedBetween(any(), any());
    }

}
