package biz.craftware.homework.core.domain.product;


import biz.craftware.homework.controller.product.dto.ProductDto;
import biz.craftware.homework.core.service.product.ProductService;
import biz.craftware.homework.exceptions.InvalidRequestException;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import javax.transaction.Transactional;
import java.util.List;

import static biz.craftware.homework.core.domain.DomainTestsUtil.createProductDto;

@SpringBootTest
@Sql("/integrationTestData.sql")
@ActiveProfiles({"test"})
@Transactional
public class ProductServiceIntegrationTest {

    public static final String NEW_NAME = "New name";
    public static final String NOT_EXISTANT_SKU = "not existant sku";

    @Autowired
    private ProductService productService;

    @Test
    public void shouldGetAllProducts() {
        //given

        //when
        List<ProductDto> result = productService.getAllProducts();

        //then
        Assertions.assertThat(result).isNotNull();
        Assertions.assertThat(result).isNotEmpty();
        Assertions.assertThat(result).hasSize(8);
    }

    @Test
    public void shouldInsertProduct() {
        //given
        ProductDto productDto = createProductDto();

        //when
        ProductDto product = productService.addProduct(productDto);
        List<ProductDto> result = productService.getAllProducts();

        //then
        Assertions.assertThat(product).isNotNull();
        Assertions.assertThat(result).hasSize(9);
    }

    @Test
    public void shouldThrowProductSkuAlreadyPresentException() {
        //given
        List<ProductDto> products = productService.getAllProducts();
        ProductDto productDto = products.get(0);

        //when
        InvalidRequestException exception = org.junit.jupiter.api.Assertions.assertThrows(InvalidRequestException.class, () -> productService.addProduct(productDto));

        //then
        Assertions.assertThat(exception).hasMessage(String.format(InvalidRequestException.SKU_IS_ALREADY_PRESENT, productDto.getSku()));
    }

    @Test
    public void shouldUpdateExistingProduct() {
        //given
        List<ProductDto> products = productService.getAllProducts();
        ProductDto productDto = products.get(0);

        //when
        productDto.setName(NEW_NAME);
        ProductDto responseDto = productService.updateProduct(productDto, productDto.getSku());

        //then
        Assertions.assertThat(responseDto).isNotNull();
        Assertions.assertThat(responseDto.getName()).isEqualTo(NEW_NAME);
    }

    @Test
    public void shouldThrowCreateNoProductSkuPresentExceptionWhenUpdating() {

        //given
        ProductDto productDto = createProductDto();
        productDto.setSku(NOT_EXISTANT_SKU);

        //when
        InvalidRequestException exception = org.junit.jupiter.api.Assertions.assertThrows(InvalidRequestException.class,
                () -> productService.updateProduct(productDto, productDto.getSku()));

        //then
        Assertions.assertThat(exception).hasMessage(String.format(InvalidRequestException.SKU_IS_NOT_PRESENT, productDto.getSku()));
    }

    @Test
    public void shouldDeleteProduct() {
        //given
        List<ProductDto> products = productService.getAllProducts();
        ProductDto productDto = products.get(0);

        //when
        productService.deleteProduct(productDto.getSku());
        List<ProductDto> results = productService.getAllProducts();

        //then
        Assertions.assertThat(results).isNotEmpty();
        Assertions.assertThat(results).hasSize(7);
    }

    @Test
    public void shouldThrowCreateNoProductSkuPresentExceptionWhenDeleting() {

        //given
        ProductDto productDto = createProductDto();
        productDto.setSku(NOT_EXISTANT_SKU);

        //when
        InvalidRequestException exception = org.junit.jupiter.api.Assertions.assertThrows(InvalidRequestException.class, () -> productService.deleteProduct(productDto.getSku()));

        //then
        Assertions.assertThat(exception).hasMessage(String.format(InvalidRequestException.SKU_IS_NOT_PRESENT, productDto.getSku()));
    }
}
