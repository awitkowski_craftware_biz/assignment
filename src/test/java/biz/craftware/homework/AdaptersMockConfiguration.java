package biz.craftware.homework;

import biz.craftware.homework.controller.order.OrderRestController;
import biz.craftware.homework.core.service.order.OrderService;
import biz.craftware.homework.core.service.order.OrderServiceImpl;
import biz.craftware.homework.core.service.product.ProductService;
import biz.craftware.homework.core.service.product.ProductServiceImpl;
import biz.craftware.homework.repository.OrderRepository;
import biz.craftware.homework.repository.ProductRepository;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;

@TestConfiguration
public class AdaptersMockConfiguration {

    @MockBean
    ProductRepository productRepository;

    @MockBean
    OrderRepository orderRepository;

    @Bean
    public ProductService testProductService(ProductRepository productRepository) {
        return new ProductServiceImpl(productRepository);
    }

    @Bean
    public OrderService testOrderService(ProductRepository productRepository, OrderRepository orderRepository){
        return new OrderServiceImpl(orderRepository, productRepository);
    }
}
