package biz.craftware.homework.controller.product;

import biz.craftware.homework.controller.product.dto.ProductDto;
import biz.craftware.homework.core.service.product.ProductService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/products")
public class ProductRestController {

    private ProductService productService;

    public ProductRestController(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping
    public ResponseEntity<ProductDto> addProduct(@RequestBody ProductDto productDto) {
        return ResponseEntity.ok(productService.addProduct(productDto));
    }

    @GetMapping()
    public ResponseEntity<List<ProductDto>> getAllProducts() {
        return ResponseEntity.ok(productService.getAllProducts());
    }

    @PatchMapping("/{sku}")
    public ResponseEntity<ProductDto> updateProduct(@PathVariable("sku") String productSku, @RequestBody ProductDto productDto) {
        return ResponseEntity.ok(productService.updateProduct(productDto, productSku));
    }

    @DeleteMapping("/{sku}")
    public void deleteProduct(@PathVariable("sku") String productSku) {
        productService.deleteProduct(productSku);
    }

}
