package biz.craftware.homework.controller.product.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@Builder
public class ProductDto {

    private String name;
    private String sku;
    private BigDecimal price;
    private LocalDate dateCreated;

}
