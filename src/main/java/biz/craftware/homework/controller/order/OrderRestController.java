package biz.craftware.homework.controller.order;

import biz.craftware.homework.controller.order.dto.OrderDto;
import biz.craftware.homework.controller.order.dto.OrderDtoNoId;
import biz.craftware.homework.core.service.order.OrderService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/orders")
public class OrderRestController {

    private OrderService orderService;

    public OrderRestController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping
    public ResponseEntity<OrderDto> addOrder(@RequestBody OrderDtoNoId orderDto) {
        OrderDto order = orderService.addOrder(orderDto);
        return ResponseEntity.ok(order);
    }

    @GetMapping(path = "/{uid}/sum")
    public ResponseEntity<BigDecimal> getOrderTotalSum(@PathVariable("uid") String uid) {
        BigDecimal orderAmount = orderService.getTotalOrderAmount(uid);
        return ResponseEntity.ok(orderAmount);
    }

    @GetMapping
    public ResponseEntity<List<OrderDto>> getAllOrdersByDates(@RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)  LocalDateTime from,
                                                              @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)  LocalDateTime to) {
        List<OrderDto> orders = orderService.getAllOrders(from, to);
        return ResponseEntity.ok(orders);
    }
}
