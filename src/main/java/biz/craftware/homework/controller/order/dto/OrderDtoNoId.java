package biz.craftware.homework.controller.order.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;
import java.util.Set;

@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
public class OrderDtoNoId {

    private LocalDateTime orderCreated;
    private Set<String> products;
    private String cusomterEmail;

}
