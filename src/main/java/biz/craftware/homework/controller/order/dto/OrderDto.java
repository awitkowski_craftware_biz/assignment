package biz.craftware.homework.controller.order.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class OrderDto extends OrderDtoNoId {

    private String orderId;
}
