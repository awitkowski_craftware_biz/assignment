package biz.craftware.homework.core.service.product;

import biz.craftware.homework.controller.product.dto.ProductDto;

import java.util.List;

public interface ProductService {

    ProductDto addProduct(ProductDto productDto);

    List<ProductDto> getAllProducts();

    ProductDto updateProduct(ProductDto productDto, String productSku);

    void deleteProduct(String sku);
}
