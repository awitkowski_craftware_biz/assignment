package biz.craftware.homework.core.service.order;

import biz.craftware.homework.controller.order.dto.OrderDto;
import biz.craftware.homework.controller.order.dto.OrderDtoNoId;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public interface OrderService {

    OrderDto addOrder(OrderDtoNoId orderDto);
    BigDecimal getTotalOrderAmount(String orderId);
    List<OrderDto> getAllOrders(LocalDateTime from, LocalDateTime to);

}
