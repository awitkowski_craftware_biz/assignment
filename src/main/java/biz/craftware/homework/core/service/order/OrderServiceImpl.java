package biz.craftware.homework.core.service.order;

import biz.craftware.homework.controller.order.dto.OrderDto;
import biz.craftware.homework.controller.order.dto.OrderDtoNoId;
import biz.craftware.homework.core.domain.Order;
import biz.craftware.homework.core.domain.Product;
import biz.craftware.homework.exceptions.InvalidRequestException;
import biz.craftware.homework.repository.OrderRepository;
import biz.craftware.homework.repository.ProductRepository;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class OrderServiceImpl implements OrderService {

    private static int SHORT_ID_LENGTH = 8;

    private OrderRepository orderRepository;
    private ProductRepository productRepository;

    public OrderServiceImpl(OrderRepository orderRepository, ProductRepository productRepository) {
        this.orderRepository = orderRepository;
        this.productRepository = productRepository;
    }

    @Override
    public OrderDto addOrder(OrderDtoNoId orderDto) {

        validateProductAssignment(orderDto);

        Order order = createOrder(orderDto);
        Order savedOrder = orderRepository.save(order);

        OrderDto response = createOrderDto(savedOrder);

        return response;
    }

    @Override
    @Transactional
    public BigDecimal getTotalOrderAmount(String orderId) {

        BigDecimal orderAmount = orderRepository.findByOrderUid(orderId)
                .orElseThrow(() -> InvalidRequestException.createNoOrderIdPresent(orderId))
                .computeTotalOrderAmount();

        return orderAmount;
    }

    @Override
    @Transactional
    public List<OrderDto> getAllOrders(final LocalDateTime from, final LocalDateTime to) {
        List<Order> orders = orderRepository.getAllByDateCreatedBetween(from, to);

        List<OrderDto> response = orders.stream().map(this::createOrderDto).collect(Collectors.toList());
        return response;
    }

    private void validateProductAssignment(final OrderDtoNoId orderDto) {
        productRepository.findAllBySkuInAndOrderIsNotNull(orderDto.getProducts())
                .ifPresent(products -> {
                    throw InvalidRequestException.createProductAlreadyOrdered();
                });
    }

    private Set<Product> getAllProductsBySkus(final Set<String> skus) {
        Set<Product> products = productRepository.findAllBySkuInAndDeletedIsFalse(skus);
        return products;
    }

    private Order createOrder(final OrderDtoNoId orderDto) {
        Set<Product> products = getAllProductsBySkus(orderDto.getProducts());

        Order order = Order.builder()
                .orderUid(RandomStringUtils.randomAlphanumeric(SHORT_ID_LENGTH))
                .customerEmail(orderDto.getCusomterEmail())
                .dateCreated(LocalDateTime.now())
                .build();

        order.addProducts(products);

        return order;
    }

    private OrderDto createOrderDto(final Order order) {
        OrderDto orderDto = OrderDto.builder()
                .orderCreated(order.getDateCreated())
                .orderId(order.getOrderUid())
                .cusomterEmail(order.getCustomerEmail())
                .products(order.getProducts().stream().map(o -> o.getSku()).collect(Collectors.toSet()))
                .build();
        return orderDto;
    }
}
