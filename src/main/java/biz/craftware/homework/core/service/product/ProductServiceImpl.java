package biz.craftware.homework.core.service.product;

import biz.craftware.homework.controller.product.dto.ProductDto;
import biz.craftware.homework.core.domain.Product;
import biz.craftware.homework.exceptions.InvalidRequestException;
import biz.craftware.homework.repository.ProductRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class ProductServiceImpl implements ProductService {

    private ProductRepository productRepository;

    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public ProductDto addProduct(ProductDto productDto) {

        validateProductsSkuUniqueness(productDto);

        ProductDto responseDto = createProductDto(saveProduct(() -> createProduct(productDto)));
        return responseDto;
    }

    public List<ProductDto> getAllProducts() {

        List<ProductDto> responseDtos =
                productRepository.findAllByDeletedIsFalse().stream().map(this::createProductDto).collect(Collectors.toList());

        return responseDtos;
    }

    public ProductDto updateProduct(ProductDto productDto, String productSku) {

        Product product = validateProductSkuPresenceAndGet(productSku);
        ProductDto responseDto = createProductDto(saveProduct(() -> updateProduct(product, productDto)));

        return responseDto;
    }

    public void deleteProduct(String sku) {
        Product product = validateProductSkuPresenceAndGet(sku);
        saveProduct(() -> product.markProductAsDeleted());
    }

    private Product saveProduct(Supplier<Product> productSupplier) {
        Product product = productSupplier.get();
        Product savedProduct = productRepository.save(product);
        return savedProduct;
    }

    private Product createProduct(final ProductDto productDto) {
        return Product.builder().sku(productDto.getSku())
                .name(productDto.getName())
                .price(productDto.getPrice())
                .dateCreated(LocalDate.now())
                .deleted(false).build();
    }

    private ProductDto createProductDto(final Product product) {
        return ProductDto.builder()
                .dateCreated(product.getDateCreated())
                .sku(product.getSku())
                .name(product.getName())
                .price(product.getPrice()).build();
    }

    private Product updateProduct(final Product product, final ProductDto productDto) {
        product.setName(productDto.getName());
        product.setPrice(productDto.getPrice());
        product.setSku(productDto.getSku());
        return product;
    }

    private void validateProductsSkuUniqueness(final ProductDto productDto) {
        productRepository.findBySku(productDto.getSku())
                .ifPresent(product -> {
                    throw InvalidRequestException.createProductSkuAlreadyPresent(productDto.getSku());
                });
    }

    private Product validateProductSkuPresenceAndGet(final String sku) {
        return productRepository.findBySku(sku).orElseThrow(() -> InvalidRequestException.createNoProductSkuPresent(sku));
    }

}
