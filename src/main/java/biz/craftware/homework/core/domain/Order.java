package biz.craftware.homework.core.domain;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "cust_order")
public class Order extends BaseEntity{

    @OneToMany(mappedBy = "order")
    private Set<Product> products;

    private String orderUid;
    private String customerEmail;
    private LocalDateTime dateCreated;

    public BigDecimal computeTotalOrderAmount() {
        BigDecimal totalOrderPrice = products.stream().map(Product::getPrice).reduce(BigDecimal.ZERO, BigDecimal::add);
        return totalOrderPrice;
    }

    public void addProducts(Set<Product> products) {
        this.products = products;
        products.forEach(product -> product.setOrder(this));
    }

}
