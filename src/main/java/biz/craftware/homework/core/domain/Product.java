package biz.craftware.homework.core.domain;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Product extends BaseEntity {

    @Column(nullable = false, unique = true)
    private String sku;

    @Column(name = "date_created")
    private LocalDate dateCreated;
    private String name;

    private BigDecimal price;

    private Boolean deleted;

    @ManyToOne
    @JoinColumn(name = "order_id")
    private Order order;

    public Product markProductAsDeleted() {
        setDeleted(true);
        return this;
    }
}
