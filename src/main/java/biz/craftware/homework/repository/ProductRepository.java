package biz.craftware.homework.repository;

import biz.craftware.homework.core.domain.Product;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface ProductRepository extends CrudRepository<Product, Long> {

    List<Product> findAllByDeletedIsFalse();
    Optional<Product> findBySku(final String sku);

    Set<Product> findAllBySkuInAndDeletedIsFalse(Set<String> skus);
    Optional<List<Product>> findAllBySkuInAndOrderIsNotNull(Set<String> skus);

}
