package biz.craftware.homework.repository;

import biz.craftware.homework.core.domain.Order;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface OrderRepository extends CrudRepository<Order, Long> {

    List<Order> getAllByDateCreatedBetween(final LocalDateTime from, final LocalDateTime to);
    Optional<Order> findByOrderUid(final String orderUid);
}

