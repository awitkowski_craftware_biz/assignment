package biz.craftware.homework.exceptions;

public class InvalidRequestException extends RuntimeException {

    public static final String SKU_IS_ALREADY_PRESENT = "The given sku: %s is already present";
    public static final String SKU_IS_NOT_PRESENT = "The given sku: %s is not present in database";
    public static final String SKU_IS_IN_ANOTHER_ORDER = "Some of product skus are in already placed order";

    public static final String ORDER_ID_IS_NOT_PRESENT = "The given order id: %s is not present in database";


    public InvalidRequestException(String s) {
        super(s);
    }


    public static InvalidRequestException createProductSkuAlreadyPresent(String sku) {
        InvalidRequestException invalidRequestException =
                new InvalidRequestException(String.format(SKU_IS_ALREADY_PRESENT, sku));
        return invalidRequestException;
    }

    public static InvalidRequestException createNoProductSkuPresent(String sku) {
        InvalidRequestException invalidRequestException =
                new InvalidRequestException(String.format(SKU_IS_NOT_PRESENT, sku));
        return invalidRequestException;
    }

    public static InvalidRequestException createNoOrderIdPresent(String orderId) {
        InvalidRequestException invalidRequestException =
                new InvalidRequestException(String.format(ORDER_ID_IS_NOT_PRESENT, orderId));
        return invalidRequestException;
    }

    public static InvalidRequestException createProductAlreadyOrdered() {
        InvalidRequestException invalidRequestException =
                new InvalidRequestException(String.format(SKU_IS_IN_ANOTHER_ORDER));
        return invalidRequestException;
    }
}
