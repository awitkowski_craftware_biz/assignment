package biz.craftware.homework;

import biz.craftware.homework.core.domain.Product;
import biz.craftware.homework.core.service.order.OrderService;
import biz.craftware.homework.core.service.order.OrderServiceImpl;
import biz.craftware.homework.core.service.product.ProductService;
import biz.craftware.homework.core.service.product.ProductServiceImpl;
import biz.craftware.homework.repository.OrderRepository;
import biz.craftware.homework.repository.ProductRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class InfrastructureConfiguration {

    @Bean
    public ProductService productService(ProductRepository productRepository) {
        ProductService productService = new ProductServiceImpl(productRepository);
        return productService;
    }

    @Bean
    public OrderService orderService(ProductRepository productRepository, OrderRepository orderRepository) {
        OrderService orderService = new OrderServiceImpl(orderRepository, productRepository);
        return orderService;
    }

}
